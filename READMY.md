##Stwórz:
 + abstrakcyjną klasęAnimal, która posiada 3 atrybuty:int id1,string nameorazfloat weight. Klasa mazawierać metodęvoid 
 introduce(), która ma ”przedstawiać dane zwierzę” (patrz oczekiwany output),
 + interfejsCaninez jedną metodąvoid bark(),
 + klasęBear, która dziedziczy po klasieAnimali ma dodatkowy atrybutint furLength,
 + klasęTiger, która dziedziczy po klasieAnimali ma dodatkowy atrybutint clawLength,
 + klasęWolf, która dziedziczy po klasieAnimal, implementuje interfejsCaninei wywołując jego metodę mawypisać ”BARK BARK BARK”. Pondato,Wolfma dodatkowy atrybutint fangLength,
 + klasęDog, która dziedziczy po klasieAnimal, implementuje interfejsCaninei wywołując jego metodę ma wypi-sać ”bark bark bark”. Pondato,Dogma dodatkową metodęvoid sitPretty(), która wypisuje na standardowewyjście ”(name)2sits pretty.”.
 
 
  Następnie, w metodziemainwykonaj następujące operacje:
 + stwórz instancję klasyBear- imię: Yogi, waga: 200, długość futra: 40,
 + stwórz instancję klasyTiger- imię: Jataka, waga: 150, długość pazura: 25,
 + stwórz instancję klasyWolf- imię: Howler, waga: 70, długość futra: 40,
 + stwórz instancję klasyDog- imię: Scooby, waga: 30,
 + stwórz listę animalsi dodaj tam wszystkie zwierzęta. Dla każdego obiektu wypisz jegoid, a następnie ma sięono przedstawić,
 + stwórz listęhowlersi dodaj tam Scooby’ego oraz Howlera. Dla każdego obiektu whowlers, wypisz ”My na-me is (name) and I am barking ”, następnie wywołaj metodęhowl. Potem, jeśli to możliwe, wywołaj metodęsitPretty()dla każdego obiektu
 
 ##Oczekiwany output:
 1 : I’m a bear. My name is Yogi. I weigh 200.0 kg and my fur length is 40.
 
 2 : I’m a tiger. My name is Jataka. I weigh 150.0 kg and my claw length is 25.
 
 3 : I’m a wolf. My name is Howler. I weigh 70.0 kg and my fang length is 40.
 
 4 : I’m a dog. My name is Scooby. I weigh 30.0 kg.
 
 My name is Howler and I am barking: BARK BARK BARK
 
 My name is Scooby and I am barking: bark bark bark
 
 Scooby sits pretty.