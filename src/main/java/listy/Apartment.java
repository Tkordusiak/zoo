package listy;

public class Apartment {

    private String city;
    private int area;
    private int price;

    public String getCity() {
        return city;
    }

    public int getArea() {
        return area;
    }

    public int getPrice() {
        return price;
    }

    public Apartment(String city, int area, int price) {
        this.city = city;
        this.area = area;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Apartment in " + city + " coast " + getFullPrice();
    }

    float getFullPrice() {
        return (float) (price * 0.95);
    }
}
