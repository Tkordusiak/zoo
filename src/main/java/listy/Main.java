package listy;

import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Apartment> apartments = new LinkedList<>();
        apartments.add(new Apartment("Warszawa", 55, 5000));
        apartments.add(new Apartment("Kraków", 100, 100000));
        apartments.add(new Apartment("Gliwcie", 250, 25000));

        for (Apartment apert : apartments){
            System.out.println(apert);
        }

    }
}
