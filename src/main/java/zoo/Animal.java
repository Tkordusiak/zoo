package zoo;

import java.util.stream.Stream;

public abstract class Animal {
    int id;
    String name;
    float weight;


    public Animal(int id, String name, float weight) {
        this.id = id;
        this.name = name;
        this.weight = weight;
    }


    abstract void introduce();

//    void introduce(){
//        System.out.println( "My name is " + name + "I weigth " + weight);
//    }

}
