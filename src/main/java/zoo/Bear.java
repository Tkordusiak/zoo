package zoo;

import java.util.stream.Stream;

public class Bear extends Animal {

    int furLength;


    public Bear(int id, String name, float weight, int furLength) {
        super(id, name, weight);
        this.furLength = furLength;
    }


    @Override
    void introduce() {
        System.out.println(id + ". " + "I'm bear. My name is " + name +
                " I weight " + weight + " kg" +
                " and my fur length " + furLength);
    }
}
