package zoo;

import java.util.stream.Stream;

public class Dog extends Animal implements Canine {


    public Dog(int id, String name, float weight) {
        super(id, name, weight);
    }

    @Override
    void introduce() {
        System.out.println(id + ". " + "I'm dog. My name is " + name + " I weight " + weight + " kg");
    }

    @Override
    public void bark() {
        System.out.println("My name is " + name +"bark, bark, bark");

    }

    void sitPretty() {
        System.out.println(name + " sit Pretty");

    }
}
