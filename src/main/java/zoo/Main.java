package zoo;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Bear bear = new Bear(1, "Yogi", 200, 42);
        Tiger tiger = new Tiger(2, "Jataka", 100, 23);
        Wolf wolf = new Wolf(3, "Howler", 150, 25);
        Dog dog = new Dog(4, "Scooby", 50);

        bear.introduce();
        tiger.introduce();
        wolf.introduce();
        dog.introduce();
        wolf.bark();
        dog.bark();
        dog.sitPretty();

        List<Animal> animals = new ArrayList<>();
        animals.add(new Bear(1, "Yogi", 200, 45));
        for (Animal animal: animals) {
            System.out.println(animals);
        }



    }
}
