package zoo;

import java.util.stream.Stream;

public class Tiger extends Animal {

    int clawLength;


    public Tiger(int id, String name, float weight, int clawLength) {
        super(id, name, weight);
        this.clawLength = clawLength;
    }

    @Override
    void introduce() {
        System.out.println(id + ". " + "I'm tiger. My name is " + name +
                " I weight " + weight + " kg" +
                " and my fur length " + clawLength);
    }
}
