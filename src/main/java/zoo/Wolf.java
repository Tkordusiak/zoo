package zoo;

import java.util.stream.Stream;

public class Wolf extends Animal implements Canine {
    int fangLength;

    public Wolf(int id, String name, float weight, int fangLength) {
        super(id, name, weight);
        this.fangLength = fangLength;
    }


    @Override
    public void bark() {
        System.out.println("My name is " + name + "I am barking: BARK, BARK, BARK");
    }

    @Override
    void introduce() {
        System.out.println(id + ". " + "I'm wolf. My name is " + name +
                " I weight " + weight + " kg" +
                " and my fur length " + fangLength);
    }
}
